package com.pack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetwebJenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetwebJenkinsApplication.class, args);
	}

}
